#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#define PORT 8080

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread,b;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    //set socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully created\n");

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully set\n");

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    if(bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully binded\n");

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully listening\n");

    if ((new_socket = accept(server_fd, (struct sockaddr *)NULL, NULL))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully accepted\n");
    //recieve filename from client
    valread = read( new_socket , buffer, 1024);
    FILE *fp = fopen(buffer, "wb");
        while((b = recv(new_socket, buffer, 1024, 0)) > 0){
            if(fwrite(buffer, 1, b, fp) < 0){
                printf("Error writing to file\n");
                return -1;
            }
            bzero(buffer, 1024);
    }
    fclose(fp);
    printf("File successfully received\n");
    close(new_socket);
    close(server_fd);
    return 0; 
}

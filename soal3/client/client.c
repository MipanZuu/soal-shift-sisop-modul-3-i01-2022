#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <syscall.h>
#include <sys/stat.h>
#include <stdbool.h>  
#include <fcntl.h>
#define PORT 8080



int main(int argc, char **argv) {


    system("zip hartakarun.zip -r /home/zufarrifqi/shift3/hartakarun");
    
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    if(argc != 3){
        perror("Invalid number of arguments\n");
        return -1;
    }
    if(strcmp(argv[1],"send") != 0){
        perror("Invalid command\n");
        return -1;
    }
    if(access(argv[2], F_OK) == -1){
        perror("File not found\n");
        return -1;
    }
    char *filename = argv[2];
    //make socket for client
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("\n Socket creation error \n");
        return -1;
    }
    printf("socket created\n");

    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        perror("\nInvalid address/ Address not supported \n");
        return -1;
    }
    printf("address created\n");

    int con = connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    if(con < 0){
        perror("Connection failed\n");
        return -1;
    }
    printf("Connected to the server\n");
    
    FILE *fp = fopen(filename, "r");
    if(fp == NULL){
        perror("Error in reading file\n");
        return -1;
    }
    
    send(sock, filename, strlen(filename), 0);
    sleep(1);
    while((valread = fread(buffer, 1, sizeof(buffer), fp))> 0){
        if(send(sock, buffer, valread, 0) < 0){
            perror("Error in sending file\n");
            return -1;
        }
        bzero(buffer, sizeof(buffer));
    }
    printf("File successfully sent\n");
    fclose(fp);
    close(sock);
}





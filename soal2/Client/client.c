#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#define PORT 8080
#define MAX_BUFFER 1024

int response_state = 0;
int is_login = 0;
char *username;

struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;
char buffer[1024] = {0};

// prototypes
void start_sock_client();
void reset_buffer();
void input_send_text();
void get_print_message();
void get_resp();
void send_login_state();
void download_to_client(char source_file[1024], char target_file[1024]);
void download_problem();

  
int main(int argc, char const *argv[]) {
    puts("Client starting\n");
    start_sock_client();

    while(response_state >= 0)
    {
        get_resp();

        if(response_state == 1)
            input_send_text();
        else if(response_state == 2)
            get_print_message();
        else if(response_state == 3)
            send_login_state();
        else if(response_state == 4)
            is_login = 1;
        else if(response_state == 5)
            download_problem();

        if(response_state < 0){
            exit(EXIT_SUCCESS);
        }
    }

    puts("\nClient exiting\n");

    return 0;
}

void start_sock_client()
{
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        exit(EXIT_FAILURE);
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        exit(EXIT_FAILURE);
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        exit(EXIT_FAILURE);
    }
}

void reset_buffer()
{
    for(int i=0; i<=1024; i++){
        buffer[i] = 0;
    }
}

void input_send_text()
{
    fgets(buffer,MAX_BUFFER,stdin);

    if ((strlen(buffer) > 0) && (buffer[strlen (buffer) - 1] == '\n')){
        buffer[strlen (buffer) - 1] = '\0';
    }

    send(sock , buffer , strlen(buffer) , 0 );
    reset_buffer();
}

void get_resp()
{
    // receive response_state here
    //puts("Waiting response from server\n");
    read( sock , buffer, 1024);

    response_state = atoi(buffer);
    //printf("Received response [%d]\n", response_state);
    reset_buffer();
}

void get_print_message()
{
    //puts("awaiting");
    read( sock , buffer, 1024);
    puts(buffer);
    reset_buffer();
}

void send_login_state()
{
    sprintf(buffer,"%d",is_login);
    send(sock, buffer, strlen(buffer), 0);
    reset_buffer();
}

void download_to_client(char source_file[1024], char target_file[1024])
{
    char ch;
    FILE *source, *target;  

    source = fopen(source_file, "r");
    if (source == NULL) {
        puts("File not found!");
        exit(EXIT_FAILURE);
    }
    target = fopen(target_file, "w+");

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}

void download_problem()
{
    char title[1024], server_path[1024], *p;

    read( sock , buffer, 1024);

    p = strtok(buffer,"\t");
    strcpy(title,p);
    p = strtok(NULL,"\0");
    strcpy(server_path,p);
    reset_buffer();

    mkdir(title,0777);

    char src_desc_path[1024],src_input_path[1024],dest_desc_path[1024],dest_input_path[1024];
    sprintf(src_desc_path,"%s/%s/description.txt",server_path,title);
    //puts(src_desc_path);
    sprintf(src_input_path,"%s/%s/input.txt",server_path,title);
    //puts(src_input_path);
    sprintf(dest_desc_path,"./%s/description.txt",title);
    //puts(dest_desc_path);
    sprintf(dest_input_path,"./%s/input.txt",title);
    //puts(dest_input_path);

    download_to_client(src_desc_path,dest_desc_path);
    download_to_client(src_input_path,dest_input_path);
}
#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#define PORT 8080

pthread_t tid[2],tid_cli[10];
int mutex = 0, serving_tid = 0, fix_runaway_thread = 0;

int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char buffer[1024] = {0};


void start_sock_server();
void reset_buffer();
int get_login_state(int i);
void send_resp(int response_state);
void send_message(char message[]);
void *send_message_core(void *arg);
void get_input();
void startup_screen(char *user);
int check_password(char password[]);
void register_user();
void login_user(char *user);
void create_database();
void main_screen(char *user);
void add_soj(char *user);
void download_to_server(char source_file[1024], char target_file[1024]);
void see_soj();
void download_soj(char *title);
void submit_soj(char *input);
void *run_server(void *arg);

int main(int argc, char const *argv[]) {
    puts("Server starting\n");
    start_sock_server();
    create_database();
    int totalThreads = 0;
    int *arg = malloc(sizeof(*arg));

    while(1){
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
        }
        *arg = totalThreads;
        pthread_create(&(tid[totalThreads]), NULL, &run_server, arg);
        totalThreads++;
        while(serving_tid != totalThreads);
        sleep(1);
    }

    return 0;
}


void start_sock_server()
{
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }


}

void *run_server(void *arg)
{
    int i = *((int *) arg);

    printf("client [%d] entered with thread id %d\n\n",i,pthread_self());

    while(serving_tid != i);
    char user[1024] = {0};
    int login_state;
    while(1){
        if(fix_runaway_thread){
            puts("removing runaway thread");
            fix_runaway_thread = 0;
            break;
        }

        login_state = get_login_state(i);
        sleep(1);
        if(!login_state){
            startup_screen(user);
        }else if(login_state){
            main_screen(user);
        }
    }
}

void reset_buffer()
{
    for(int i=0; i<=1024; i++){
        buffer[i] = 0;
    }
}

void send_resp(int response_state)
{
    sprintf(buffer,"%d",response_state);
    //puts(buffer);
    send(new_socket, buffer, strlen(buffer), 0);
    reset_buffer();

    if(response_state == -1){
        reset_buffer();
        close(new_socket);
        sleep(1);
        serving_tid++;
        //puts("Server exiting\n");
        fix_runaway_thread = 1;
        printf("Cancelling thread tid_cli[%d]\n\n",serving_tid-1);
        pthread_cancel(tid_cli[serving_tid-1]);
    }
}

void *send_message_core(void *arg)
{
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid[0])){
        mutex = 0;
        send_resp(2);
        mutex = 2;
    }
    else if(pthread_equal(id,tid[1])){
        while(mutex != 2);
        char *message = (char *) arg; 
        //puts(message);
        send(new_socket, message, strlen(message), 0);
        mutex = 0;
    }
}

void send_message(char message[])
{
    int err;
    err = pthread_create(&(tid[0]),NULL,send_message_core,NULL);
    if(err){
        printf("\n can't create thread 1 : [%s]",strerror(err));
    }

    err = pthread_create(&(tid[1]),NULL,send_message_core,(void *) message);
    if(err){
        printf("\n can't create thread 2 : [%s]",strerror(err));
    }

    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
}

void get_input()
{
    sleep(1);
    send_resp(1);
    valread = read( new_socket, buffer, 1024);
}

int get_login_state(int i)
{
    send_resp(3);
    valread = read(new_socket , buffer, 1024);

    int buf = atoi(buffer);
    printf("Client [%d] current Login state = %d\n\n",i,buf);
    reset_buffer();

    return buf;
}

void startup_screen(char *user)
{
    char command[1024];
    send_message("[Simple Online Judge]\nUser is not logged in.\n\nInput an action (register/login/exit):");
    sleep(1);
    get_input();
    strcpy(command,buffer);

    printf("Inputted command = %s\n\n",command);

    if(!strcmp(command,"login")){
        login_user(user);
    }else if(!strcmp(command,"register")){
        register_user();
    }else if(!strcmp(command,"exit")){
        puts("client exiting");
        send_resp(-1);
    }else{
        send_message("\nInvalid command, please try again.\n");
    }
    
    reset_buffer();
}

int check_password(char password[])
{

    int checker, pass_length;
    pass_length = strlen(password);
    checker = 0;

    if(pass_length < 6){
        return 1;
    }

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 48 && (int)password[i] <= 57){
            checker = 1;
            break;
        }
    }

    if(!checker) return 1;
    checker = 0;

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 97 && (int)password[i] <= 122){
            checker = 1;
            break;
        }
    }

    if(checker == 0) return 1;
    checker = 0;

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 65 && (int)password[i] <= 90){
            checker = 1;
            break;
        }
    }

    if(checker == 0) return 1;
    return 0;
}

void register_user()
{
    char username[1024],password[1024];
    int pass = 0;

    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;
    fp  = fopen ("users.txt", "a+");

    while(!pass){
        send_message("\nInput username:");

        get_input();
        strcpy(username, buffer);

        pass = 1;

        while ((read = getline(&line, &len, fp)) != -1) {
            line[strcspn(line, ":")] = 0;
            if (!strcmp(line, username)) {
                pass = 0;
                break;
            }
        }

        if(!pass){
            send_message("\n[[This username has already been taken! Try another.]]\n");
            puts("[[Username Rejected]]\n");
        }else{
            puts("[[Username Accepted]]\n");
        }
    }

    pass = 0;

    while(!pass){
        send_message("Input password:");

        get_input();
        strcpy(password, buffer);

        if(check_password(password) !=0){
            send_message("\n[[Password does not meet criteria! Please try again.]]\n");
            puts("[[Password Rejected]]\n");
        }else{
            send_message(" ");
            puts("[[Password Accepted]]\n");
            pass = 1;
        }
    }
    fprintf(fp,"%s:%s\n",username,password);
    fclose (fp);


    sleep(1);

}

void login_user(char *user)
{
    char username[1024],password[1024];
    int pass = 0;

    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;

    while(!pass){
        char buf[2048] = {0};
        send_message("\nInput username:");
        get_input();
        strcpy(username, buffer);
        reset_buffer();

        send_message("Input password:");
        get_input();
        strcpy(password, buffer);
        reset_buffer();

        sprintf(buf,"%s:%s",username,password);

        fp  = fopen ("users.txt", "a+");
        while ((read = getline(&line, &len, fp)) != -1) {
            line[strcspn(line, "\n")] = 0;
            if (!strcmp(line, buf)) {
                pass = 1;
                break;
            }
        }
        fclose(fp);
        

        if(!pass){
            send_message("\n[[Invalid Username or Password. Try again.]]");
            puts("test");
            printf("[[Login Failed, %s no match]]\n\n",buf);
        }else{
            sleep(1);
            puts("[[Login Successful]]\n");
            strcpy(user,username);
            send_resp(4);
            sleep(1);
        }
    }
}

void create_database()
{
    FILE *fp;
    fp  = fopen ("problem.tsv", "a+");
    fclose(fp);
}

void main_screen(char *user)
{
    char command[1024], bufmsg[1024];
    char *tok_input;
    sprintf(bufmsg,"\nWelcome to SOJ %s!\n\nInput an action(add/see/download <problem-title>/submit <problem-title> <output-path>/exit):",user);
    send_message(bufmsg);
    get_input();
    
    strcpy(command,buffer);
    reset_buffer();

    if(!strcmp(command,"add")){
        puts("in add\n");
        add_soj(user);
    }else if(!strcmp(command,"see")){
        puts("in see");
        see_soj();
    }else if(!strcmp(command,"exit")){
        puts("client exiting");
        send_resp(-1);
    }else{
        tok_input = strtok(command," ");

        if(!strcmp(tok_input,"download")){
            puts("in download");
            tok_input = strtok(NULL,"\0");
            download_soj(tok_input);
        }else if(!strcmp(tok_input,"submit")){
            puts("in submit");
            tok_input = strtok(NULL,"\0");
            submit_soj(tok_input);
        }else{
        send_message("\nInvalid command, please try again.\n");
        }
    }

    for(int i=0; i<=1024; i++){
        command[i] = 0;
    }
    
    sleep(1);
}

void add_soj(char *user)
{
    char title[1024],desc_path[1024],input_path[1024],output_path[1024];

    send_message("\nProblem Name:");
    get_input();
    strcpy(title, buffer);
    reset_buffer();

    send_message("\nFilepath description.txt:");
    get_input();
    strcpy(desc_path, buffer);
    reset_buffer();

    send_message("\nFilepath input.txt:");
    get_input();
    strcpy(input_path, buffer);
    reset_buffer();

    send_message("\nFilepath output.txt:");
    get_input();
    strcpy(output_path, buffer);
    reset_buffer();

    printf("[Inputted Parameter]\nProblem Name:\t\t\t%s\nFilepath description.txt:\t%s\nFilepath input.txt:\t\t%s\nFilepath output.txt:\t\t%s\n\n",title,desc_path,input_path,output_path);

    FILE *fp;
    fp  = fopen ("problem.tsv", "a+");
    fprintf(fp,"%s\t%s\n",title,user);
    fclose(fp);

    mkdir(title,0777);

    char desc_path_new[1024],input_path_new[1024],output_path_new[1024];
    sprintf(desc_path_new,"./%s/description.txt",title);
    sprintf(input_path_new,"./%s/input.txt",title);
    sprintf(output_path_new,"./%s/output.txt",title);

    download_to_server(desc_path,desc_path_new);
    download_to_server(input_path,input_path_new);
    download_to_server(output_path,output_path_new);
    
}

void download_to_server(char source_file[1024], char target_file[1024])
{
    char ch;
    FILE *source, *target;  

    source = fopen(source_file, "r");
    if (source == NULL) {
        puts("File not found!");
        exit(EXIT_FAILURE);
    }
    target = fopen(target_file, "w+");

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}

void see_soj()
{
    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;
    fp  = fopen ("problem.tsv", "r");

    char title[1024],author[1024], *p;
    char buf[2048] = "\n";

    while((read = getline(&line, &len, fp)) != -1) {
        p = strtok(line,"\t");
        strcpy(title,p);
        p= strtok(NULL,"\0");
        strcpy(author,p);
        sprintf(buffer,"%s by %s\n",title,author);
        strcat(buf,buffer);
        reset_buffer();
    }

    send_message(buf);
}

void download_soj(char *title)
{
    send_resp(5);
    sleep(1);

    char cwd[1024],msg[2048];
    getcwd(cwd,sizeof(cwd));
    sprintf(msg,"%s\t%s\0",title,cwd);

    puts(msg);

    send(new_socket, msg, strlen(msg), 0);

    sleep(1);
}

void submit_soj(char *input)
{
    char *buf;
    buf = strrchr(input,' ');

    char title[1024],client_path[1024],server_path[1024];
    strncpy(title,input,(int)(buf-input));
    title[(int)(buf-input)] = '\0';
    strcpy(client_path,buf+1);
    sprintf(server_path,"./%s/output.txt",title);

    puts(title);
    puts(client_path);

    FILE *fp1 = fopen(client_path, "r");
    FILE *fp2 = fopen(server_path, "r");

    char ch1 = getc(fp1);
    char ch2 = getc(fp2);
    int pass = 1;
    
    while (ch1 != EOF && ch2 != EOF){
        if (ch1 != ch2){
            pass = 0;
            break;
        }
        ch1 = getc(fp1);
        ch2 = getc(fp2);
    }

    if(pass){
        send_message("\nAC\n");
    }else{
        send_message("\nWA\n");
    }

    fclose(fp1);
    fclose(fp2);

    sleep(1);
}
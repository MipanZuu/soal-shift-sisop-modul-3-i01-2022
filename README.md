# soal-shift-sisop-modul-3-I01-2022

Group Members:

1. Denta Bramasta Hidayat (5025201116)
2. Muhammad Fatih Akbar (5025201117)
3. Muhammad Zufarrifqi Prakoso (5025201276)

## Important Links

+ [Questions](https://docs.google.com/document/d/1w3tYGgqIkHRFoqKJ9nKzwGLCbt30HM7S/edit)
+ [Soal 1 Answer](https://gitlab.com/MipanZuu/soal-shift-sisop-modul-3-i01-2022/-/blob/main/soal1/soal1.c)
+ [Soal 2 Answer](https://gitlab.com/MipanZuu/soal-shift-sisop-modul-3-i01-2022/-/tree/main/soal2)
+ [Soal 3 Answer](https://gitlab.com/MipanZuu/soal-shift-sisop-modul-3-i01-2022/-/tree/main/soal3)

## Table of Contents

- [soal-shift-sisop-modul-3-I01-2022](#soal-shift-sisop-modul-3-i01-2022)
  - [Important Links](#important-links)
  - [Table of Contents](#table-of-contents)
- [Soal 1](#soal-1)
  - [Question](#question)
  - [soal1.c](#soal1c)
  - [Explanation](#explanation)
    - [1A Downloaded files from drive that we saved on modul. used thread to run the function at the same time.](#1a-downloaded-files-from-drive-that-we-saved-on-modul-used-thread-to-run-the-function-at-the-same-time)
    - [1B. Decode the file of music.txt and quote.txt using base64 and using thread to run at the same time.](#1b-decode-the-file-of-musictxt-and-quotetxt-using-base64-and-using-thread-to-run-at-the-same-time)
    - [1C Move the result of music&quote.txt to folder hasil.](#1c-move-the-result-of-musicquotetxt-to-folder-hasil)
    - [1D zip hasil folder with passowrd protection "mihinomenest(namauser)"](#1d-zip-hasil-folder-with-passowrd-protection-mihinomenestnamauser)
    - [1E unzip the zip and also add no.txt inside of hasil directory.](#1e-unzip-the-zip-and-also-add-notxt-inside-of-hasil-directory)
    - [int main function](#int-main-function)
  - [Result](#result)
  - [kendala](#kendala)
- [Soal 2](#soal-2)
  - [Question](#question-1)
  - [Codings](#codings)
    - [client.c](#clientc)
    - [server.c](#serverc)
  - [Explanation](#explanation-1)
    - [client.c](#clientc-1)
    - [2a. Registration & Logging In](#2a-registration--logging-in)
      - [startup_screen()](#startup_screen)
      - [register_user()](#register_user)
      - [check_password()](#check_password)
      - [login_user()](#login_user)
      - [Output](#output)
    - [2b. Creating the problem database.](#2b-creating-the-problem-database)
    - [2c. `add`](#2c-add)
      - [download_to_server()](#download_to_server)
      - [Output](#output-1)
    - [2d. `see`](#2d-see)
      - [Output](#output-2)
    - [2e. `download < problem-title >`](#2e-download--problem-title-)
      - [download_problem()](#download_problem)
      - [Output](#output-3)
    - [2f. `submit < problem-title > < path-to-output >`](#2f-submit--problem-title---path-to-output-)
      - [Output](#output-4)
    - [2g. Multiple Client Handling](#2g-multiple-client-handling)
    - [Output:](#output-5)
  - [Revisions Made](#revisions-made)
- [Soal 3](#soal-3)
  - [Question](#question-2)
  - [soal3.c](#soal3c)
  - [client.c](#clientc-2)
  - [server.c](#serverc-1)
  - [Explanation](#explanation-2)
    - [3a. Extract zip to /home/[user]/shift3/ and categorize it by its extension recursively](#3a-extract-zip-to-homeusershift3-and-categorize-it-by-its-extension-recursively)
    - [3b. categorize unknown file and hidden file](#3b-categorize-unknown-file-and-hidden-file)
    - [3c Move the file using thread](#3c-move-the-file-using-thread)
    - [3d ZIP file from shift3/hartakarun](#3d-zip-file-from-shift3hartakarun)
    - [3e sent file from client to server using "send hartakarun.zip"](#3e-sent-file-from-client-to-server-using-send-hartakarunzip)
  - [Revisi](#revisi)



# Soal 1

## Question

- Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

- Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

- Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

- Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

- Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

## soal1.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <syslog.h>
#include <pwd.h>
#include <grp.h>
#include <stdint.h>
#include <string.h>

pthread_t tid[4];
pid_t child;

#define WHITESPACE 64
#define EQUALS 65
#define INVALID 66


static const unsigned char d[] = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 66, 66, 66, 65, 66, 66, 66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66, 66, 66, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66
    };

int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen){
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end){
        unsigned char c = d[*in++];

        switch (c){
        case WHITESPACE:
            continue; /* skip whitespace */
        case INVALID:
            return 1; /* invalid input, return error */
        case EQUALS:  /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iteration
            /* If the buffer is full, split it into bytes */
            if (iter == 4){
                if ((len += 3) > *outLen)
                    return 1; /* buffer overflow */
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3){
        if ((len += 2) > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2){
        if (++len > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}



void norepeatfork (char execute[], char *variable_path[])
{
    int status;
    pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
        execv(execute, variable_path);
    }

    else
    {
        ((wait(&status))>0);
        return;
    }
}


void *unzipQuotes( void *ptr){
    char destination[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote";
    char quotefold[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/quote.zip";
    char quote[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote";

    pid_t child_id;
    char path[201];
    int status;
    child_id = fork();

    strcpy(path, destination);
    strcat(path, "/quote");



    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldquo[] = {"mkdir","-p", quote, NULL};
        norepeatfork("/bin/mkdir", createfoldquo);

        char *unzipTheQ[] = {"unzip", "-q", quotefold, "-d", destination, NULL};
        norepeatfork("/usr/bin/unzip", unzipTheQ);
    }
    else {
        while ((wait(&status))>0);
    }
    return NULL;
}

void *unzipMusic(void *ptr){
    char destination[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music";
    char musicfold[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/music.zip";
    char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music";
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldmus[] = {"mkdir","-p", music, NULL};
        norepeatfork("/bin/mkdir", createfoldmus);

        char *unzipTheM[] = {"unzip", "-q", musicfold, "-d", destination, NULL};
        norepeatfork("/usr/bin/unzip", unzipTheM);
    }
    else {
        while ((wait(&status))>0);
    }
}

// void *decodeMusic(void *ptr){
//     FILE* fp;
//     char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt";
//     char destinationm[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/";
//     char path[151];
//     strcpy(path, destinationm);
//     strcat(path, "music.txt");
//     fp = fopen(path, "a");

//     for(int i=1; i<10; i++){
//     char *decode[]={"base64 -d /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/m%d.txt >> /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt", i};
//     norepeatfork("/usr/bin/base64", decode);
//     }
//     // for(int i=1; i<10; i++){
//     //         char cmd[120];
//     //         sprintf(cmd, "base64 -d /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/q%d.txt >> /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/quote.txt", i);
//     //         base64(cmd);
//     //     }
// }

// void *decodeQuote(void *ptr){
//     FILE* fp;
//     char quote[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/quote.txt";
//     char destinationq[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/";
//     char path[151];
//     strcpy(path, destinationq);
//     strcat(path, "quote.txt");
//     fp = fopen(path, "a");

//     char *decode[]={"base64", "-d", destinationq, NULL};
//     norepeatfork("/usr/bin/base64", decode);

//     fprintf(fp, "%s\n", decode);
// }

// void base64(char* text){
//     int status;
//     pid_t child_id = fork();

//     if(child_id < 0){
//         exit(EXIT_FAILURE);
//     }
//     else if(child_id == 0){
//         execl("/bin/sh", "sh", "-c", text, (char *)0);
//     }
//     else{
//         while((wait(&status)) > 0);
//     }
// }

// void newLine(char* dir){
//     int status;
//     pid_t child_id = fork();

//     if(child_id < 0){
//         exit(EXIT_FAILURE);
//     }
//     else if(child_id == 0){
//         char text[100] = "echo "" >> ";
//         strcat(text, dir);
//         execl("/bin/sh", "sh", "-c", text, (char *)0);
//     }
//     else{
//         while((wait(&status)) > 0);
//     }
// }

void decodeBase64(char *name){
    DIR *dp;
    if ((dp = opendir(name)) != NULL){
        struct dirent **namelist;
        int n;

        n = scandir(name, &namelist, NULL, alphasort);
        int i = 0;
        if (n == -1)
            perror("scandir");

        for (int i = 0; i < n; i++){
            if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
                char *decoded;
                char dirName[100];
                strcpy(dirName, name);
                strcat(dirName, "/");
                strcat(dirName, namelist[i]->d_name);
                free(namelist[i]);

                FILE *fp = fopen(dirName, "r");
                char line[100];
                while (fgets(line, 100, fp) != NULL){
                    char *in = line;
                    size_t inLen = strlen(in);
                    size_t outLen = inLen;
                    decoded = malloc(sizeof(char) * 32768);
                    base64decode(in, inLen, (unsigned char *)decoded, &outLen);
                }
                fclose(fp);

                char txtDir[100];
                strcpy(txtDir, name);
                strcat(txtDir, "/");
                strcat(txtDir, name);
                strcat(txtDir, ".txt");

                fp = fopen(txtDir, "a");
                fprintf(fp, "%s\n", decoded);
                fclose(fp);
            }
        }
        free(namelist);
        closedir(dp);
    }
}

void *decodeMainM(){
        decodeBase64("music");
    return NULL;
}

void *decodeMainQ(){
        decodeBase64("quote");
    return NULL;
}


// void* decode(){
//     pthread_t id = pthread_self();

//     if(pthread_equal(id, tid[2])){
//     FILE* fp;
//     char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt";
//     char destinationm[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/";
//     char path[151];
//     strcpy(path, destinationm);
//     strcat(path, "music.txt");
//     fp = fopen(path, "a");
//         for(int i=1; i<10; i++){
//             char cmd[120];
//             sprintf(cmd, "base64 -d /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/m%d.txt >> /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt", i);
//             base64(cmd);
//             // newLine("/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt");
//         }
//     }
//     else if(pthread_equal(id, tid[3])){
//         for(int i=1; i<10; i++){
//             char cmd[120];
//             sprintf(cmd, "base64 -d /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/q%d.txt >> /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/quote.txt", i);
//             base64(cmd);
//             newLine("/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/quote.txt");
//         }
//     }
// }


void *Createhasil(){
    char hasil[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil";
    pid_t child_id;
    char path[201];
    int status;
    child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldmus[] = {"mkdir","-p", hasil, NULL};
        norepeatfork("/bin/mkdir", createfoldmus);

    }
    else {
        while ((wait(&status))>0);
    }
}

void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "mv", "{}", destination, ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void *next(){
    pid_t child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char hasil[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil";
        char quote[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/";
        char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/";
        move(quote, hasil, "quote.txt");
        move(music, hasil, "music.txt");
    }
    else{
        wait(NULL);
    }
}

void zip(char *name, char *password){
    char fileName[100];
    strcpy(fileName, name);
    strcat(fileName, ".zip");

    char src[100];
    strcpy(src, name);
    strcat(src, "/*");

    char *argv[] = {"zip", "-P", password, "-r", "-j", fileName, name, NULL};
    execv("/usr/bin/zip", argv);
}

void DeleteDirectory(char *name){
    char *argv[] = {"rm", "-rf", name, NULL};
    execv("/bin/rm", argv);
}

void addingNo(char *name){
    FILE *fp = fopen(name, "w");
    fprintf(fp, "No\n");
    fclose(fp);
}

void unzipWithPassword(char *fileName, char *password){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", "-P", password, fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}


void *zipMain(){
    char password[100] = "mihinomenest";
    char *user = getenv("USER");
    strcat(password, user);

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[6])){
        int status;
        pid_t child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                zip("hasil", password);

            else{
                while ((wait(&status)) > 0);
                pid_t child_id2 = fork();

                if (child_id2 == 0)
                    DeleteDirectory("hasil");

                else{
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 == 0)
                        unzipWithPassword("hasil.zip", password);

                    else{
                        while ((wait(&status)) > 0);
                        addingNo("hasil/no.txt");
                        zip("hasil", password);
                        DeleteDirectory("hasil");
                    }
                }
            }
        }
    }
    return NULL;
}





// void zipp(){
//     char hasil[101] = "hasil";
//     char hasildel[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil";
//     char password[101]="test123";
//     pid_t child_id = fork();
//     if (child_id < 0)
//     {
//         exit(EXIT_FAILURE);
//     }
//     if(child_id == 0){
//         char *zip[]={"zip", "--password", password, "-r", "hasil.zip", hasil, NULL};
//         norepeatfork("/usr/bin/zip", zip);

//         rmdir(hasil);

//         char *rem[]={"rm", "-rf", hasildel, NULL};
//         norepeatfork("/bin/rm", rem);
//     }
//     else{
//         wait(NULL);
//     }
// }

// void unzipadd(){
//     char destination[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil.zip";
//     pid_t child_id;
//     int status;
//     child_id = fork();

//     if (child_id < 0)
//     {
//         exit(EXIT_FAILURE);
//     }
//     if (child_id == 0)
//     {
//         char *unzipTheM[] = {"unzip", destination, NULL};
//         norepeatfork("/usr/bin/unzip", unzipTheM);
//     }
//     else {
//         while ((wait(&status))>0);
//     }
// }


int main () {
    int thread;
    thread = pthread_create(&tid[0], NULL, unzipQuotes, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 0\n");

    thread = pthread_create(&tid[1], NULL, unzipMusic, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }

    printf("pthread_create() success for thread 1\n");

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    thread = pthread_create(&tid[2], NULL, decodeMainM, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 2\n");


    thread = pthread_create(&tid[3], NULL, decodeMainQ, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 3 \n");
    pthread_join(tid[2], NULL);
    pthread_join(tid[3], NULL);

    thread = pthread_create(&tid[4], NULL, Createhasil, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 4 \n");

    thread = pthread_create(&tid[5], NULL, next, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 5 \n");
    pthread_join(tid[4], NULL);
    pthread_join(tid[5], NULL);

    thread = pthread_create(&tid[6], NULL, zipMain, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 6 \n");
    pthread_join(tid[6], NULL);

    return 0;
}
```

## Explanation

### 1A Downloaded files from drive that we saved on modul. used thread to run the function at the same time.

```c
void *unzipQuotes( void *ptr){
    char destination[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote";
    char quotefold[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/quote.zip";
    char quote[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote";

    pid_t child_id;
    char path[201];
    int status;
    child_id = fork();

    strcpy(path, destination);
    strcat(path, "/quote");



    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldquo[] = {"mkdir","-p", quote, NULL};
        norepeatfork("/bin/mkdir", createfoldquo);

        char *unzipTheQ[] = {"unzip", "-q", quotefold, "-d", destination, NULL};
        norepeatfork("/usr/bin/unzip", unzipTheQ);
    }
    else {
        while ((wait(&status))>0);
    }
    return NULL;
}

void *unzipMusic(void *ptr){
    char destination[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music";
    char musicfold[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/music.zip";
    char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music";
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldmus[] = {"mkdir","-p", music, NULL};
        norepeatfork("/bin/mkdir", createfoldmus);

        char *unzipTheM[] = {"unzip", "-q", musicfold, "-d", destination, NULL};
        norepeatfork("/usr/bin/unzip", unzipTheM);
    }
    else {
        while ((wait(&status))>0);
    }
}

// In main function
thread = pthread_create(&tid[0], NULL, unzipQuotes, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 0\n");

    thread = pthread_create(&tid[1], NULL, unzipMusic, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }

    printf("pthread_create() success for thread 1\n");
```

As usual, I use unzip function from execv to unzip the zip. create a char to store the destionation of zip and also the destination where the result stored.

in main function there is a thread to run the function at the same time and there is if else function to know when the thread is succcessful or not.

### 1B. Decode the file of music.txt and quote.txt using base64 and using thread to run at the same time.

```c
static const unsigned char d[] = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 66, 66, 66, 65, 66, 66, 66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66, 66, 66, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66
    };
```

there is static const unsigned char d for change the words to basic words (decode)

```c
int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen){
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end){
        unsigned char c = d[*in++];

        switch (c){
        case WHITESPACE:
            continue; /* skip whitespace */
        case INVALID:
            return 1; /* invalid input, return error */
        case EQUALS:  /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iteration
            /* If the buffer is full, split it into bytes */
            if (iter == 4){
                if ((len += 3) > *outLen)
                    return 1; /* buffer overflow */
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3){
        if ((len += 2) > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2){
        if (++len > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}
```

this function is where the text will be doceded using base 64.

```c
void decodeBase64(char *name){
    DIR *dp;
    if ((dp = opendir(name)) != NULL){
        struct dirent **namelist;
        int n;

        n = scandir(name, &namelist, NULL, alphasort);
        int i = 0;
        if (n == -1)
            perror("scandir");

        for (int i = 0; i < n; i++){
            if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
                char *decoded;
                char dirName[100];
                strcpy(dirName, name);
                strcat(dirName, "/");
                strcat(dirName, namelist[i]->d_name);
                free(namelist[i]);

                FILE *fp = fopen(dirName, "r");
                char line[100];
                while (fgets(line, 100, fp) != NULL){
                    char *in = line;
                    size_t inLen = strlen(in);
                    size_t outLen = inLen;
                    decoded = malloc(sizeof(char) * 32768);
                    base64decode(in, inLen, (unsigned char *)decoded, &outLen);
                }
                fclose(fp);

                char txtDir[100];
                strcpy(txtDir, name);
                strcat(txtDir, "/");
                strcat(txtDir, name);
                strcat(txtDir, ".txt");

                fp = fopen(txtDir, "a");
                fprintf(fp, "%s\n", decoded);
                fclose(fp);
            }
        }
        free(namelist);
        closedir(dp);
    }
}

void *decodeMainM(){
        decodeBase64("music");
    return NULL;
}

void *decodeMainQ(){
        decodeBase64("quote");
    return NULL;
}
```

this is the main funtion of base64 where we assign the destination of file (quote.music).txt and also using thread to run the funtion at the same time.

### 1C Move the result of music&quote.txt to folder hasil.

```c
void *Createhasil(){
    char hasil[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil";
    pid_t child_id;
    char path[201];
    int status;
    child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldmus[] = {"mkdir","-p", hasil, NULL};
        norepeatfork("/bin/mkdir", createfoldmus);

    }
    else {
        while ((wait(&status))>0);
    }
}
```

this function is to create new directory folder called "hasil", using mkdir to create directory inside of folder soal1

```c
void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "mv", "{}", destination, ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void *next(){
    pid_t child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char hasil[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil";
        char quote[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/";
        char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/";
        move(quote, hasil, "quote.txt");
        move(music, hasil, "music.txt");
    }
    else{
        wait(NULL);
    }
}
```

on the move fucntion is to move folder using mv command. then, on the nextfunction is the main function to move folder using move function to folder hasil.

### 1D zip hasil folder with passowrd protection "mihinomenest(namauser)"

```c
void zip(char *name, char *password){
    char fileName[100];
    strcpy(fileName, name);
    strcat(fileName, ".zip");

    char src[100];
    strcpy(src, name);
    strcat(src, "/*");

    char *argv[] = {"zip", "-P", password, "-r", "-j", fileName, name, NULL};
    execv("/usr/bin/zip", argv);
}
```

inside zi function there us name and password for password protection using zip command and read the password to protect the zip

### 1E unzip the zip and also add no.txt inside of hasil directory.

```c
void unzipWithPassword(char *fileName, char *password){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", "-P", password, fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}
```

unzipWithpassword function we unzip hasil.zip first using that function (zip command)"

```c
void addingNo(char *name){
    FILE *fp = fopen(name, "w");
    fprintf(fp, "No\n");
    fclose(fp);
}
```

then adding no.txt inside of hasil/no.txt

```c
void DeleteDirectory(char *name){
    char *argv[] = {"rm", "-rf", name, NULL};
    execv("/bin/rm", argv);
}
```

this deletedirectory is to delete old hasil directory for not duplicate the directory

```c
void *zipMain(){
    char password[100] = "mihinomenest";
    char *user = getenv("USER");
    strcat(password, user);

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[6])){
        int status;
        pid_t child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                zip("hasil", password);

            else{
                while ((wait(&status)) > 0);
                pid_t child_id2 = fork();

                if (child_id2 == 0)
                    DeleteDirectory("hasil");

                else{
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 == 0)
                        unzipWithPassword("hasil.zip", password);

                    else{
                        while ((wait(&status)) > 0);
                        addingNo("hasil/no.txt");
                        zip("hasil", password);
                        DeleteDirectory("hasil");
                    }
                }
            }
        }
    }
    return NULL;
}
```

this unzip main is to unzip/zip the directory with passowrd.

### int main function

```c
int main () {
    int thread;
    thread = pthread_create(&tid[0], NULL, unzipQuotes, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 0\n");

    thread = pthread_create(&tid[1], NULL, unzipMusic, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }

    printf("pthread_create() success for thread 1\n");

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    thread = pthread_create(&tid[2], NULL, decodeMainM, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 2\n");


    thread = pthread_create(&tid[3], NULL, decodeMainQ, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 3 \n");
    pthread_join(tid[2], NULL);
    pthread_join(tid[3], NULL);

    thread = pthread_create(&tid[4], NULL, Createhasil, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 4 \n");

    thread = pthread_create(&tid[5], NULL, next, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 5 \n");
    pthread_join(tid[4], NULL);
    pthread_join(tid[5], NULL);

    thread = pthread_create(&tid[6], NULL, zipMain, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 6 \n");
    pthread_join(tid[6], NULL);

    return 0;
}
```

## Result

thread result

![](https://i.ibb.co/4N4zxH3/Screen-Shot-2022-04-16-at-22-09-13.png)

directory soal1

![](https://i.ibb.co/xGK97fx/Screen-Shot-2022-04-16-at-22-10-16.png)

when we want to unzip hasil.zip

![](https://i.ibb.co/9GrBSQp/Screen-Shot-2022-04-16-at-22-10-38.png)

directory hasil

![](https://i.ibb.co/VtfH9b3/Screen-Shot-2022-04-16-at-22-10-21.png)

inside of music.txt

![](https://i.ibb.co/yhy2ZKx/Screen-Shot-2022-04-16-at-22-10-24.png[)

inside of quote.txt

![](https://i.ibb.co/vmjjJsC/Screen-Shot-2022-04-16-at-22-10-27.png)

inside of no.txt

![](https://i.ibb.co/THtTrXh/Screen-Shot-2022-04-16-at-22-10-32.png)

## kendala

- tidak bisa decode dengan base64 command line jadi menggunakan function biasa di c.
- masih belum selesai dengan 1E. (revisi)

# Soal 2

## Question

> Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judgesederhana

The question furthers explain that the online judge system should be capable of these things:

+ Uses a client-server system with socket programming
+ Client that have **not logged in** is able to:
  + **Register** a new account into the system, which are stored into `user.txt`.
  + **Login** into the system
+ Client that have **logged in** is able to inut the following commands:
  + **add**, used to add problems into the system.
  + **see**, used to see all available problems in the system.
  + **download < problem-title >**, used to download the problem description and input into the client.
  + **submit < problem-title > < path-to-output >**, used to check if the client's output matches with the server's output of the problem.
+ Server can handle multiple clients

## Codings

### client.c

```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#define PORT 8080
#define MAX_BUFFER 1024

int response_state = 0;
int is_login = 0;
char *username;

struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;
char buffer[1024] = {0};

// prototypes
void start_sock_client();
void reset_buffer();
void input_send_text();
void get_print_message();
void get_resp();
void send_login_state();
void download_to_client(char source_file[1024], char target_file[1024]);
void download_problem();

  
int main(int argc, char const *argv[]) {
    puts("Client starting\n");
    start_sock_client();

    while(response_state >= 0)
    {
        get_resp();

        if(response_state == 1)
            input_send_text();
        else if(response_state == 2)
            get_print_message();
        else if(response_state == 3)
            send_login_state();
        else if(response_state == 4)
            is_login = 1;
        else if(response_state == 5)
            download_problem();

        if(response_state < 0){
            exit(EXIT_SUCCESS);
        }
    }

    puts("\nClient exiting\n");

    return 0;
}

void start_sock_client()
{
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        exit(EXIT_FAILURE);
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        exit(EXIT_FAILURE);
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        exit(EXIT_FAILURE);
    }
}

void reset_buffer()
{
    for(int i=0; i<=1024; i++){
        buffer[i] = 0;
    }
}

void input_send_text()
{
    fgets(buffer,MAX_BUFFER,stdin);

    if ((strlen(buffer) > 0) && (buffer[strlen (buffer) - 1] == '\n')){
        buffer[strlen (buffer) - 1] = '\0';
    }

    send(sock , buffer , strlen(buffer) , 0 );
    reset_buffer();
}

void get_resp()
{
    // receive response_state here
    //puts("Waiting response from server\n");
    read( sock , buffer, 1024);

    response_state = atoi(buffer);
    //printf("Received response [%d]\n", response_state);
    reset_buffer();
}

void get_print_message()
{
    //puts("awaiting");
    read( sock , buffer, 1024);
    puts(buffer);
    reset_buffer();
}

void send_login_state()
{
    sprintf(buffer,"%d",is_login);
    send(sock, buffer, strlen(buffer), 0);
    reset_buffer();
}

void download_to_client(char source_file[1024], char target_file[1024])
{
    char ch;
    FILE *source, *target;  

    source = fopen(source_file, "r");
    if (source == NULL) {
        puts("File not found!");
        exit(EXIT_FAILURE);
    }
    target = fopen(target_file, "w+");

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}

void download_problem()
{
    char title[1024], server_path[1024], *p;

    read( sock , buffer, 1024);

    p = strtok(buffer,"\t");
    strcpy(title,p);
    p = strtok(NULL,"\0");
    strcpy(server_path,p);
    reset_buffer();

    mkdir(title,0777);

    char src_desc_path[1024],src_input_path[1024],dest_desc_path[1024],dest_input_path[1024];
    sprintf(src_desc_path,"%s/%s/description.txt",server_path,title);
    //puts(src_desc_path);
    sprintf(src_input_path,"%s/%s/input.txt",server_path,title);
    //puts(src_input_path);
    sprintf(dest_desc_path,"./%s/description.txt",title);
    //puts(dest_desc_path);
    sprintf(dest_input_path,"./%s/input.txt",title);
    //puts(dest_input_path);

    download_to_client(src_desc_path,dest_desc_path);
    download_to_client(src_input_path,dest_input_path);
}
```

### server.c

```c
#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#define PORT 8080

pthread_t tid[2],tid_cli[10];
int mutex = 0, serving_tid = 0, fix_runaway_thread = 0;

int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char buffer[1024] = {0};


void start_sock_server();
void reset_buffer();
int get_login_state(int i);
void send_resp(int response_state);
void send_message(char message[]);
void *send_message_core(void *arg);
void get_input();
void startup_screen(char *user);
int check_password(char password[]);
void register_user();
void login_user(char *user);
void create_database();
void main_screen(char *user);
void add_soj(char *user);
void download_to_server(char source_file[1024], char target_file[1024]);
void see_soj();
void download_soj(char *title);
void submit_soj(char *input);
void *run_server(void *arg);

int main(int argc, char const *argv[]) {
    puts("Server starting\n");
    start_sock_server();
    create_database();
    int totalThreads = 0;
    int *arg = malloc(sizeof(*arg));

    while(1){
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
        }
        *arg = totalThreads;
        pthread_create(&(tid[totalThreads]), NULL, &run_server, arg);
        totalThreads++;
        while(serving_tid != totalThreads);
        sleep(1);
    }

    return 0;
}


void start_sock_server()
{
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }


}

void *run_server(void *arg)
{
    int i = *((int *) arg);

    printf("client [%d] entered with thread id %d\n\n",i,pthread_self());

    while(serving_tid != i);
    char user[1024] = {0};
    int login_state;
    while(1){
        if(fix_runaway_thread){
            puts("removing runaway thread");
            fix_runaway_thread = 0;
            break;
        }

        login_state = get_login_state(i);
        sleep(1);
        if(!login_state){
            startup_screen(user);
        }else if(login_state){
            main_screen(user);
        }
    }
}

void reset_buffer()
{
    for(int i=0; i<=1024; i++){
        buffer[i] = 0;
    }
}

void send_resp(int response_state)
{
    sprintf(buffer,"%d",response_state);
    //puts(buffer);
    send(new_socket, buffer, strlen(buffer), 0);
    reset_buffer();

    if(response_state == -1){
        reset_buffer();
        close(new_socket);
        sleep(1);
        serving_tid++;
        //puts("Server exiting\n");
        fix_runaway_thread = 1;
        printf("Cancelling thread tid_cli[%d]\n\n",serving_tid-1);
        pthread_cancel(tid_cli[serving_tid-1]);
    }
}

void *send_message_core(void *arg)
{
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid[0])){
        mutex = 0;
        send_resp(2);
        mutex = 2;
    }
    else if(pthread_equal(id,tid[1])){
        while(mutex != 2);
        char *message = (char *) arg; 
        //puts(message);
        send(new_socket, message, strlen(message), 0);
        mutex = 0;
    }
}

void send_message(char message[])
{
    int err;
    err = pthread_create(&(tid[0]),NULL,send_message_core,NULL);
    if(err){
        printf("\n can't create thread 1 : [%s]",strerror(err));
    }

    err = pthread_create(&(tid[1]),NULL,send_message_core,(void *) message);
    if(err){
        printf("\n can't create thread 2 : [%s]",strerror(err));
    }

    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
}

void get_input()
{
    sleep(1);
    send_resp(1);
    valread = read( new_socket, buffer, 1024);
}

int get_login_state(int i)
{
    send_resp(3);
    valread = read(new_socket , buffer, 1024);

    int buf = atoi(buffer);
    printf("Client [%d] current Login state = %d\n\n",i,buf);
    reset_buffer();

    return buf;
}

void startup_screen(char *user)
{
    char command[1024];
    send_message("[Simple Online Judge]\nUser is not logged in.\n\nInput an action (register/login/exit):");
    sleep(1);
    get_input();
    strcpy(command,buffer);

    printf("Inputted command = %s\n\n",command);

    if(!strcmp(command,"login")){
        login_user(user);
    }else if(!strcmp(command,"register")){
        register_user();
    }else if(!strcmp(command,"exit")){
        puts("client exiting");
        send_resp(-1);
    }else{
        send_message("\nInvalid command, please try again.\n");
    }
    
    reset_buffer();
}

int check_password(char password[])
{

    int checker, pass_length;
    pass_length = strlen(password);
    checker = 0;

    if(pass_length < 6){
        return 1;
    }

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 48 && (int)password[i] <= 57){
            checker = 1;
            break;
        }
    }

    if(!checker) return 1;
    checker = 0;

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 97 && (int)password[i] <= 122){
            checker = 1;
            break;
        }
    }

    if(checker == 0) return 1;
    checker = 0;

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 65 && (int)password[i] <= 90){
            checker = 1;
            break;
        }
    }

    if(checker == 0) return 1;
    return 0;
}

void register_user()
{
    char username[1024],password[1024];
    int pass = 0;

    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;
    fp  = fopen ("users.txt", "a+");

    while(!pass){
        send_message("\nInput username:");

        get_input();
        strcpy(username, buffer);

        pass = 1;

        while ((read = getline(&line, &len, fp)) != -1) {
            line[strcspn(line, ":")] = 0;
            if (!strcmp(line, username)) {
                pass = 0;
                break;
            }
        }

        if(!pass){
            send_message("\n[[This username has already been taken! Try another.]]\n");
            puts("[[Username Rejected]]\n");
        }else{
            puts("[[Username Accepted]]\n");
        }
    }

    pass = 0;

    while(!pass){
        send_message("Input password:");

        get_input();
        strcpy(password, buffer);

        if(check_password(password) !=0){
            send_message("\n[[Password does not meet criteria! Please try again.]]\n");
            puts("[[Password Rejected]]\n");
        }else{
            send_message(" ");
            puts("[[Password Accepted]]\n");
            pass = 1;
        }
    }
    fprintf(fp,"%s:%s\n",username,password);
    fclose (fp);


    sleep(1);

}

void login_user(char *user)
{
    char username[1024],password[1024];
    int pass = 0;

    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;

    while(!pass){
        char buf[2048] = {0};
        send_message("\nInput username:");
        get_input();
        strcpy(username, buffer);
        reset_buffer();

        send_message("Input password:");
        get_input();
        strcpy(password, buffer);
        reset_buffer();

        sprintf(buf,"%s:%s",username,password);

        fp  = fopen ("users.txt", "a+");
        while ((read = getline(&line, &len, fp)) != -1) {
            line[strcspn(line, "\n")] = 0;
            if (!strcmp(line, buf)) {
                pass = 1;
                break;
            }
        }
        fclose(fp);
        

        if(!pass){
            send_message("\n[[Invalid Username or Password. Try again.]]");
            puts("test");
            printf("[[Login Failed, %s no match]]\n\n",buf);
        }else{
            sleep(1);
            puts("[[Login Successful]]\n");
            strcpy(user,username);
            send_resp(4);
            sleep(1);
        }
    }
}

void create_database()
{
    FILE *fp;
    fp  = fopen ("problem.tsv", "a+");
    fclose(fp);
}

void main_screen(char *user)
{
    char command[1024], bufmsg[1024];
    char *tok_input;
    sprintf(bufmsg,"\nWelcome to SOJ %s!\n\nInput an action(add/see/download <problem-title>/submit <problem-title> <output-path>/exit):",user);
    send_message(bufmsg);
    get_input();
    
    strcpy(command,buffer);
    reset_buffer();

    if(!strcmp(command,"add")){
        puts("in add\n");
        add_soj(user);
    }else if(!strcmp(command,"see")){
        puts("in see");
        see_soj();
    }else if(!strcmp(command,"exit")){
        puts("client exiting");
        send_resp(-1);
    }else{
        tok_input = strtok(command," ");

        if(!strcmp(tok_input,"download")){
            puts("in download");
            tok_input = strtok(NULL,"\0");
            download_soj(tok_input);
        }else if(!strcmp(tok_input,"submit")){
            puts("in submit");
            tok_input = strtok(NULL,"\0");
            submit_soj(tok_input);
        }else{
        send_message("\nInvalid command, please try again.\n");
        }
    }

    for(int i=0; i<=1024; i++){
        command[i] = 0;
    }
    
    sleep(1);
}

void add_soj(char *user)
{
    char title[1024],desc_path[1024],input_path[1024],output_path[1024];

    send_message("\nProblem Name:");
    get_input();
    strcpy(title, buffer);
    reset_buffer();

    send_message("\nFilepath description.txt:");
    get_input();
    strcpy(desc_path, buffer);
    reset_buffer();

    send_message("\nFilepath input.txt:");
    get_input();
    strcpy(input_path, buffer);
    reset_buffer();

    send_message("\nFilepath output.txt:");
    get_input();
    strcpy(output_path, buffer);
    reset_buffer();

    printf("[Inputted Parameter]\nProblem Name:\t\t\t%s\nFilepath description.txt:\t%s\nFilepath input.txt:\t\t%s\nFilepath output.txt:\t\t%s\n\n",title,desc_path,input_path,output_path);

    FILE *fp;
    fp  = fopen ("problem.tsv", "a+");
    fprintf(fp,"%s\t%s\n",title,user);
    fclose(fp);

    mkdir(title,0777);

    char desc_path_new[1024],input_path_new[1024],output_path_new[1024];
    sprintf(desc_path_new,"./%s/description.txt",title);
    sprintf(input_path_new,"./%s/input.txt",title);
    sprintf(output_path_new,"./%s/output.txt",title);

    download_to_server(desc_path,desc_path_new);
    download_to_server(input_path,input_path_new);
    download_to_server(output_path,output_path_new);
    
}

void download_to_server(char source_file[1024], char target_file[1024])
{
    char ch;
    FILE *source, *target;  

    source = fopen(source_file, "r");
    if (source == NULL) {
        puts("File not found!");
        exit(EXIT_FAILURE);
    }
    target = fopen(target_file, "w+");

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}

void see_soj()
{
    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;
    fp  = fopen ("problem.tsv", "r");

    char title[1024],author[1024], *p;
    char buf[2048] = "\n";

    while((read = getline(&line, &len, fp)) != -1) {
        p = strtok(line,"\t");
        strcpy(title,p);
        p= strtok(NULL,"\0");
        strcpy(author,p);
        sprintf(buffer,"%s by %s\n",title,author);
        strcat(buf,buffer);
        reset_buffer();
    }

    send_message(buf);
}

void download_soj(char *title)
{
    send_resp(5);
    sleep(1);

    char cwd[1024],msg[2048];
    getcwd(cwd,sizeof(cwd));
    sprintf(msg,"%s\t%s\0",title,cwd);

    puts(msg);

    send(new_socket, msg, strlen(msg), 0);

    sleep(1);
}

void submit_soj(char *input)
{
    char *buf;
    buf = strrchr(input,' ');

    char title[1024],client_path[1024],server_path[1024];
    strncpy(title,input,(int)(buf-input));
    title[(int)(buf-input)] = '\0';
    strcpy(client_path,buf+1);
    sprintf(server_path,"./%s/output.txt",title);

    puts(title);
    puts(client_path);

    FILE *fp1 = fopen(client_path, "r");
    FILE *fp2 = fopen(server_path, "r");

    char ch1 = getc(fp1);
    char ch2 = getc(fp2);
    int pass = 1;
    
    while (ch1 != EOF && ch2 != EOF){
        if (ch1 != ch2){
            pass = 0;
            break;
        }
        ch1 = getc(fp1);
        ch2 = getc(fp2);
    }

    if(pass){
        send_message("\nAC\n");
    }else{
        send_message("\nWA\n");
    }

    fclose(fp1);
    fclose(fp2);

    sleep(1);
}
```

## Explanation

### client.c
Before getting through the points of soal 2, a context on how the clients-server system work will be needed. Most of the workings of the the system are located in the server whereas the clients only facilitate the interface and some other stuff for the server.

The clients can be broken down into the function:

+ Get input from the user then send it to the server. This is done via the `input_send_text()` function paired with `get_input()` function in the server.
+ Receive a message from server then print it out. This is done via the `get_print_message()` function paired with `send_message()` function in the server.
+ Send the clients current login state. This is done via the `send_login_state()` function paired with `get_login_state()` function in the server.
+ Changes the current login state of the client.
+ Downloads the problem after using the **download** command. This is done via the `download_problem()` function paired with `` function in the server, this will be explained further down the report.

All of this are controlled by the serer requests by sending respond code via the `send_resp()` function that is in accordance to their needs, where the client will receive this respond code via the `get_resp()` function.

```c
get_resp();

if(response_state == 1)
    input_send_text();
else if(response_state == 2)
    get_print_message();
else if(response_state == 3)
    send_login_state();
else if(response_state == 4)
    is_login = 1;
else if(response_state == 5)
    download_problem();

if(response_state < 0){
    exit(EXIT_SUCCESS);
}
```

The clients will exit if a **-1** respond code is received.

### 2a. Registration & Logging In

This part of the problem uses 2 functions namely `login_user()` & `register_user()` with the addition of 2 supporting function namely `startup_screen()` & `check_password`.

#### startup_screen()

```c
void startup_screen(char *user)
{
    char command[1024];
    send_message("[Simple Online Judge]\nUser is not logged in.\n\nInput an action (register/login/exit):");
    sleep(1);
    get_input();
    strcpy(command,buffer);

    printf("Inputted command = %s\n\n",command);

    if(!strcmp(command,"login")){
        login_user(user);
    }else if(!strcmp(command,"register")){
        register_user();
    }else if(!strcmp(command,"exit")){
        puts("client exiting");
        send_resp(-1);
    }else{
        send_message("\nInvalid command, please try again.\n");
    }
    
    reset_buffer();
}
```

This is the first thing a client would see, where in this screen the client would be able to input a **login/register/exit** command where login/register would forward it to its respective functions, whereas exit would make the server send a **-1** respond code.

#### register_user()

```c
void register_user()
{
    char username[1024],password[1024];
    int pass = 0;

    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;
    fp  = fopen ("users.txt", "a+");

    while(!pass){
        send_message("\nInput username:");

        get_input();
        strcpy(username, buffer);

        pass = 1;

        while ((read = getline(&line, &len, fp)) != -1) {
            line[strcspn(line, ":")] = 0;
            if (!strcmp(line, username)) {
                pass = 0;
                break;
            }
        }

        if(!pass){
            send_message("\n[[This username has already been taken! Try another.]]\n");
            puts("[[Username Rejected]]\n");
        }else{
            puts("[[Username Accepted]]\n");
        }
    }

    pass = 0;

    while(!pass){
        send_message("Input password:");

        get_input();
        strcpy(password, buffer);

        if(check_password(password) !=0){
            send_message("\n[[Password does not meet criteria! Please try again.]]\n");
            puts("[[Password Rejected]]\n");
        }else{
            send_message(" ");
            puts("[[Password Accepted]]\n");
            pass = 1;
        }
    }
    fprintf(fp,"%s:%s\n",username,password);
    fclose (fp);


    sleep(1);

}
```

Here the first to be done is creating/opening the file `users.txt` using the `fopen()` with the **a+** flag as we want to create if the file does not exist and read & append to it if it already exists. 

Next in the first while loop is where the username would be inputted and processed, the server would ask for the user input via `get_input()` then it would be checked for any doubles by reading `user.txt` for any matches line by line. Then in the second while loop is where the password is inputted, the inputted password would then be processed in `check_password()`.

After passing all criteria, the username and password would then be appended into a new line `users.txt` with a **:** separating the both.

#### check_password()

```c
int check_password(char password[])
{

    int checker, pass_length;
    pass_length = strlen(password);
    checker = 0;

    if(pass_length < 6){
        return 1;
    }

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 48 && (int)password[i] <= 57){
            checker = 1;
            break;
        }
    }

    if(!checker) return 1;
    checker = 0;

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 97 && (int)password[i] <= 122){
            checker = 1;
            break;
        }
    }

    if(checker == 0) return 1;
    checker = 0;

    for(int i = 0;i<pass_length;i++){
        if((int)password[i] >= 65 && (int)password[i] <= 90){
            checker = 1;
            break;
        }
    }

    if(checker == 0) return 1;
    return 0;
}
```

This checks the inputted password in accordance with problem criteria, if it checks out it will return a **0** else a **1**.

#### login_user()

```c
void login_user(char *user)
{
    char username[1024],password[1024];
    int pass = 0;

    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;

    while(!pass){
        char buf[2048] = {0};
        send_message("\nInput username:");
        get_input();
        strcpy(username, buffer);
        reset_buffer();

        send_message("Input password:");
        get_input();
        strcpy(password, buffer);
        reset_buffer();

        sprintf(buf,"%s:%s",username,password);

        fp  = fopen ("users.txt", "a+");
        while ((read = getline(&line, &len, fp)) != -1) {
            line[strcspn(line, "\n")] = 0;
            if (!strcmp(line, buf)) {
                pass = 1;
                break;
            }
        }
        fclose(fp);
        

        if(!pass){
            send_message("\n[[Invalid Username or Password. Try again.]]");
            puts("test");
            printf("[[Login Failed, %s no match]]\n\n",buf);
        }else{
            sleep(1);
            puts("[[Login Successful]]\n");
            strcpy(user,username);
            send_resp(4);
            sleep(1);
        }
    }
}
```

Here, the first thing to do is to get the username & password input from the client where then combining the two with a **:** as the same with registering, is checked for matches in the `users.txt` file.

If the login is successful, the username would then be stored by the server for further use and then a **4** respond code is sent to change the client's login status.

#### Output

> **Startup screen**
> ![Startup Screen](https://i.imgur.com/5MFphlS.png)

> **Registering**<br/>
> Username check
> ![username check](https://i.imgur.com/DPaF5g8.png)
>
> Password check
> ![password check](https://i.imgur.com/MBNzReY.png)
>
> users.txt
> ![user.txt content](https://i.imgur.com/Vq9mYxC.png)

> **Logging in**
> ![login test](https://i.imgur.com/5x99j7R.png)

### 2b. Creating the problem database.

This part of the problem is done using the `create_database()` function which is located just at the start of the `int main()` function as so once the server starts it will automatically create the database if it does not exist.

```c
void create_database()
{
    FILE *fp;
    fp  = fopen ("problem.tsv", "a+");
    fclose(fp);
}
```

### 2c. `add`

After getting the input and getting the command in the `main_screen()` function, this would then be forwarded into the `add_soj()` function.

```c
void add_soj(char *user)
{
    char title[1024],desc_path[1024],input_path[1024],output_path[1024];

    send_message("\nProblem Name:");
    get_input();
    strcpy(title, buffer);
    reset_buffer();

    send_message("\nFilepath description.txt:");
    get_input();
    strcpy(desc_path, buffer);
    reset_buffer();

    send_message("\nFilepath input.txt:");
    get_input();
    strcpy(input_path, buffer);
    reset_buffer();

    send_message("\nFilepath output.txt:");
    get_input();
    strcpy(output_path, buffer);
    reset_buffer();

    printf("[Inputted Parameter]\nProblem Name:\t\t\t%s\nFilepath description.txt:\t%s\nFilepath input.txt:\t\t%s\nFilepath output.txt:\t\t%s\n\n",title,desc_path,input_path,output_path);

    FILE *fp;
    fp  = fopen ("problem.tsv", "a+");
    fprintf(fp,"%s\t%s\n",title,user);
    fclose(fp);

    mkdir(title,0777);

    char desc_path_new[1024],input_path_new[1024],output_path_new[1024];
    sprintf(desc_path_new,"./%s/description.txt",title);
    sprintf(input_path_new,"./%s/input.txt",title);
    sprintf(output_path_new,"./%s/output.txt",title);

    download_to_server(desc_path,desc_path_new);
    download_to_server(input_path,input_path_new);
    download_to_server(output_path,output_path_new);
    
}
```
Here, the server would ask the client an input for **the problem's title, path to the problem's description text file, the path to the input file, and the path to the output file**. 

Then the title and the author (the current client's username) into the `problem.tsv` file separated with a **\t**. 

This would then be followed with the creation of a directory with the title name using `mkdir()`, where this would house the downloaded files from the client using `download_to_server()`.

#### download_to_server()

```c
void download_to_server(char source_file[1024], char target_file[1024])
{
    char ch;
    FILE *source, *target;  

    source = fopen(source_file, "r");
    if (source == NULL) {
        puts("File not found!");
        exit(EXIT_FAILURE);
    }
    target = fopen(target_file, "w+");

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}
```

This function serves to download or in mostly copy a file from one path to another.

#### Output

> ![add command output](https://i.imgur.com/CIJqN4K.png)
>
> problem.tsv
> ![problem.tsv content](https://i.imgur.com/NduXmfB.png)

### 2d. `see`

After getting the input and getting the command in the `main_screen()` function, this would then be forwarded into the `see_soj()` function.

```c
void see_soj()
{
    char    *line = NULL;
    size_t  len = 0;
    ssize_t read;

    FILE *fp;
    fp  = fopen ("problem.tsv", "r");

    char title[1024],author[1024], *p;
    char buf[2048] = "\n";

    while((read = getline(&line, &len, fp)) != -1) {
        p = strtok(line,"\t");
        strcpy(title,p);
        p= strtok(NULL,"\0");
        strcpy(author,p);
        sprintf(buffer,"%s by %s\n",title,author);
        strcat(buf,buffer);
        reset_buffer();
    }

    send_message(buf);
}
```

Here, the `problem.tsv` file would be open and then read line by line, where a `strtok()` is used to seperate the problem title and the author from the **\t** which then this would then be added into the `buf` variable. once all of the line been read, the `buf` variable is sent to the client for it to be printed.

#### Output

> ![see command output](https://i.imgur.com/kHQyTgr.png)

### 2e. `download < problem-title >`

Since this command have a parameter, a strtok in the `main_screen()` is used to separate the command with the parameter, in which the parameter (remainder of the `strtok()` operation) is forwarded to the `download_soj()` function to be processed.

```c
void download_soj(char *title)
{
    send_resp(5);
    sleep(1);

    char cwd[1024],msg[2048];
    getcwd(cwd,sizeof(cwd));
    sprintf(msg,"%s\t%s\0",title,cwd);

    puts(msg);

    send(new_socket, msg, strlen(msg), 0);

    sleep(1);
}
```

Here, the function would first send a **5** respond code to get the client `download_problem()` ready, where the title and the server's working directory is sent to the client for the `download_problem()` use.

#### download_problem()

```c
void download_problem()
{
    char title[1024], server_path[1024], *p;

    read( sock , buffer, 1024);

    p = strtok(buffer,"\t");
    strcpy(title,p);
    p = strtok(NULL,"\0");
    strcpy(server_path,p);
    reset_buffer();

    mkdir(title,0777);

    char src_desc_path[1024],src_input_path[1024],dest_desc_path[1024],dest_input_path[1024];
    sprintf(src_desc_path,"%s/%s/description.txt",server_path,title);
    //puts(src_desc_path);
    sprintf(src_input_path,"%s/%s/input.txt",server_path,title);
    //puts(src_input_path);
    sprintf(dest_desc_path,"./%s/description.txt",title);
    //puts(dest_desc_path);
    sprintf(dest_input_path,"./%s/input.txt",title);
    //puts(dest_input_path);

    download_to_client(src_desc_path,dest_desc_path);
    download_to_client(src_input_path,dest_input_path);
}
```

Here, the received parameters are separated via `strtok()`, in which after some processing and creation of a directory would then be forwarded to the `download_to_client()` function to download, exact copy to that of `download_to_server()` as explained in the previous section.

#### Output

> ![download command output](https://i.imgur.com/GR0zMRZ.png)
>
> result:
> ![result from the output](https://i.imgur.com/b2AMHaO.png)

### 2f. `submit < problem-title > < path-to-output >`

Since this command have a parameter, a strtok in the `main_screen()` is used to separate the command with the parameter, in which the parameter (remainder of the `strtok()` operation) is forwarded to the `submit_soj()` function to be processed.

```c
void submit_soj(char *input)
{
    char *buf;
    buf = strrchr(input,' ');

    char title[1024],client_path[1024],server_path[1024];
    strncpy(title,input,(int)(buf-input));
    title[(int)(buf-input)] = '\0';
    strcpy(client_path,buf+1);
    sprintf(server_path,"./%s/output.txt",title);

    puts(title);
    puts(client_path);

    FILE *fp1 = fopen(client_path, "r");
    FILE *fp2 = fopen(server_path, "r");

    char ch1 = getc(fp1);
    char ch2 = getc(fp2);
    int pass = 1;
    
    while (ch1 != EOF && ch2 != EOF){
        if (ch1 != ch2){
            pass = 0;
            break;
        }
        ch1 = getc(fp1);
        ch2 = getc(fp2);
    }

    if(pass){
        send_message("\nAC\n");
    }else{
        send_message("\nWA\n");
    }

    fclose(fp1);
    fclose(fp2);

    sleep(1);
}
```

Since there are 2 parameters, a `strrchr()` is used to get the last occurrence of ' ' (space) where it would then be separated via `strncpy()`, stored in `title[]` and `client_path[]` respectively in which after the `server_path[]` would be created.

These two paths would then be used to open the two files which are then to be compared for any mismatches. If any mismatches are present, then a "WA" message is sent to the client indicating a wrong answer, else an "AC" is sent.

#### Output

> ![submit command output](https://i.imgur.com/tY3LXuF.png)
>
> File content used in the screenshot above:
> ![file content](https://i.imgur.com/IaFNyf4.png)

### 2g. Multiple Client Handling

Multithreading is used to create multiple access point of the socket for the client. Here is as seen in the `int main()`:

```c
while(1){
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
    perror("accept");
    exit(EXIT_FAILURE);
    }
    *arg = totalThreads;
    pthread_create(&(tid[totalThreads]), NULL, &run_server, arg);
    totalThreads++;
    while(serving_tid != totalThreads);
    sleep(1);
}
```

It will wait advancing into the next thread while the serving thread (`serving_tid`) hasn't been off on the previous or current active thread. each thread will run `run_server()` function.

```c
void *run_server(void *arg)
{
    int i = *((int *) arg);

    printf("client [%d] entered with thread id %d\n\n",i,pthread_self());

    while(serving_tid != i);
    char user[1024] = {0};
    int login_state;
    while(1){
        if(fix_runaway_thread){
            puts("removing runaway thread");
            fix_runaway_thread = 0;
            break;
        }

        login_state = get_login_state(i);
        sleep(1);
        if(!login_state){
            startup_screen(user);
        }else if(login_state){
            main_screen(user);
        }
    }
}
```

An if statement is used to remove any runaway thread. This is triggered by the `fix_runwaya_thread` variable.

When stopping a client (**-1** respond code) the following snippet of code in the `send_resp()` function is used:

```c
if(response_state == -1){
    reset_buffer();
    close(new_socket);
    sleep(1);
    serving_tid++;
    //puts("Server exiting\n");
    fix_runaway_thread = 1;
    printf("Cancelling thread tid_cli[%d]\n\n",serving_tid-1);
    pthread_cancel(tid_cli[serving_tid-1]);
}
```

### Output:

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="soal2/multiclient.mp4" type="video/mp4">
  </video>
</figure>

![Youtube Mirror](https://youtu.be/Yg-Hdx3ohOM)

## Revisions Made

+ Finished 2b,2c,2d,2e,2f,2g points of the problem.

# Soal 3

## Question

- Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif
- Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.
- Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.
- Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.
- Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server

## soal3.c

```c

#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <dirent.h>

pthread_t tid[10000000];
char dest[200] = "/home/zufarrifqi/shift3";
char *zip = "home/zufarrifqi/hartakarun.zip";
int pthreadcount = 0;

typedef struct arg_struct {
    char arg1[100];
    char arg2[100];
}args;

void toLower(char s[]) {
  for(int i = 0; s[i] != '\0'; i++) {
    s[i] = tolower(s[i]);
  }
}
char *getext(char *filename) {
    char *ext = strchr(filename, '.');
    if(!ext || ext == filename)
        return NULL;
    return ext + 1;
}

void *move(void *arg){
    args *a = (args *)arg;
    char *path = a->arg1;
    char *src= a->arg2;
    char temp[200];
    char lowerext[100];
    //printf("%s\n", src);
    if(src[0] == '.'){
        mkdir("Hidden",0777);
        sprintf(temp,"Hidden/%s",src);
        rename(path,temp);
        printf("%s move to %s\n", path, temp);
    }
    else if(!getext(src)){
        mkdir("Unknown",0777);
        sprintf(temp,"Unknown/%s",src);
        rename(path,temp);
        printf("%s move to %s\n", path, temp);
    }
    else{
        char lowerext[50];
        strcpy(lowerext,getext(src));
        toLower(lowerext);
        mkdir(lowerext,0777);
        sprintf(temp,"%s/%s",lowerext,src);
        rename(path,temp);
        printf("%s move to %s\n", path, temp);
    }
    //clear lowerext
    memset(lowerext,0,sizeof(lowerext));
    memset(temp,0,sizeof(temp));
}
void join_threads(){
    for(int i = 0; i < pthreadcount; i++){
        pthread_join(tid[i], NULL);
    }
}
void categorize(const char *name,int indent)
{
    DIR *dir;
    struct dirent *entry;
    int err;
    if (!(dir = opendir(name))){
        printf("Cannot open directory '%s'\n", name);
        return;
    }
    while ((entry = readdir(dir)) != NULL) {
        char path[1024];
        if (entry->d_type == DT_DIR) {
            args *arg = malloc(sizeof(args));
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            categorize(path, indent + 2);
        }
        else{
            char cwd[100];
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            // printf("%*s- %s\n", indent, "", entry->d_name);
            args *arg = malloc(sizeof(args));
            strcpy(arg->arg1,path);
            strcpy(arg->arg2,entry->d_name);
            err = pthread_create(&tid[pthreadcount++], NULL, &move, (void *)arg);
            // err = pthread_create(&tid[pthreadcount - 2], NULL, &move, (void *)path);
            if(err){
                printf("\ncan't create thread :[%s]",strerror(err));
                continue;
            }
        }
    }
    closedir(dir);
}

void listdir(const char *name, int indent)
{
    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir(name)))
        return;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR){
            char path[1024];
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            printf("%*s[%s]\n", indent, "", entry->d_name);
            listdir(path, indent + 2);
        } else {
            printf("%*s- %s\n", indent, "", entry->d_name);
        }
    }
    closedir(dir);
}

int main(int argc, char **argv){

    mkdir(dest, 0777);
    chdir(dest);
    //perlu diganti sementara memakai system
    system("unzip -o ../hartakarun.zip");
    strcat(dest, "/hartakarun");
    chdir(dest);
    categorize(".",0);
    // listdir(".",0);
    //reset pthread
    join_threads();
}
```

## client.c

```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <syscall.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <fcntl.h>
#define PORT 8080



int main(int argc, char **argv) {


    system("zip hartakarun.zip -r /home/zufarrifqi/shift3/hartakarun");

    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    if(argc != 3){
        perror("Invalid number of arguments\n");
        return -1;
    }
    if(strcmp(argv[1],"send") != 0){
        perror("Invalid command\n");
        return -1;
    }
    if(access(argv[2], F_OK) == -1){
        perror("File not found\n");
        return -1;
    }
    char *filename = argv[2];
    //make socket for client
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("\n Socket creation error \n");
        return -1;
    }
    printf("socket created\n");

    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        perror("\nInvalid address/ Address not supported \n");
        return -1;
    }
    printf("address created\n");

    int con = connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    if(con < 0){
        perror("Connection failed\n");
        return -1;
    }
    printf("Connected to the server\n");

    FILE *fp = fopen(filename, "r");
    if(fp == NULL){
        perror("Error in reading file\n");
        return -1;
    }

    send(sock, filename, strlen(filename), 0);
    while((valread = fread(buffer, 1, sizeof(buffer), fp))> 0){
        if(send(sock, buffer, valread, 0) < 0){
            perror("Error in sending file\n");
            return -1;
        }
    }
    print("File successfully sent\n");
    fclose(fp);
    close(sock);
}

```

## server.c

```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#define PORT 8080

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread,b;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    //set socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully created\n");

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully set\n");

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    if(bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully binded\n");

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully listening\n");

    if ((new_socket = accept(server_fd, (struct sockaddr *)NULL, NULL))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    printf("Socket successfully accepted\n");
    //recieve filename from client
    valread = read( new_socket , buffer, 1024);
    FILE *fp = fopen(buffer, "wb");
        while((b = recv(new_socket, buffer, 1024, 0)) > 0){
            if(fwrite(buffer, 1, b, fp) < 0){
                printf("Error writing to file\n");
                return -1;
        }
    }
    fclose(fp);
    printf("File successfully received\n");
    close(new_socket);
    close(server_fd);
    return 0;
}
```
## Explanation

### 3a. Extract zip to /home/[user]/shift3/ and categorize it by its extension recursively

After extract zip to `/home/[user]/shift3/` we change the working directory to `shift3/hartakarun/` first. We need categorize to work recursively so that it can check the files in the folder. and so the implementation are like this

```c
if (entry->d_type == DT_DIR) {
    args *arg = malloc(sizeof(args));
    if (strcmp(entry->d_name, ".") == 0 || strcmp  (entry->d_name, "..") == 0)
        continue;
    snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
    categorize(path, indent + 2);
```

Because we need to move the file to the correct category. We need to the extension for each file using `strchr`.

```c
char *ext = strchr(filename, '.');
if(!ext || ext == filename)
    return NULL;
return ext + 1;
```

because the folder is not case sensitive we use tolower so that the extension converted into lower cases. Then we move the file according to its category using `rename`.

```c
char lowerext[50];
strcpy(lowerext,getext(src));
toLower(lowerext);
mkdir(lowerext,0777);
sprintf(temp,"%s/%s",lowerext,src);
rename(path,temp);
printf("%s move to %s\n", path, temp);
```

JPG folder for example :
![image](https://cdn.discordapp.com/attachments/783723240098889732/964469606838325328/unknown.png)

List of folder :

![list](https://cdn.discordapp.com/attachments/783723240098889732/964469891816112148/unknown.png)

### 3b. categorize unknown file and hidden file

To categorize these file we need to know characteristic of those file.Where unknown files dont have extension and hidden file have a dot at the beginning. To check if its unknown file we use getext same as before and if it return null then we make directory for Unknown file and move it using `rename`

```c
else if(!getext(src)){
    mkdir("Unknown",0777);
    sprintf(temp,"Unknown/%s",src);
    rename(path,temp);
    printf("%s move to %s\n", path, temp);
}
```

To check hidden file we need to check if `string[0]` is dot or not. and then we move it using rename.

```c
if(src[0] == '.'){
    mkdir("Hidden",0777);
    sprintf(temp,"Hidden/%s",src);
    rename(path,temp);
    printf("%s move to %s\n", path, temp);
}
```

Result of hidden and unknown folder
![Unknown](https://cdn.discordapp.com/attachments/783723240098889732/964472309064802324/unknown.png)

![Hidden](https://cdn.discordapp.com/attachments/783723240098889732/964472608299040768/unknown.png)

### 3c Move the file using thread

To move the file using thread we use `pthread_create` and and use move function to move the file according to its category. besides that we use `struct arg` to get path from each file.

```c
strcpy(arg->arg1,path);
strcpy(arg->arg2,entry->d_name);
err = pthread_create(&tid[pthreadcount++], NULL, &move, (void *)arg);
```

and then we join each thread

```c
for(int i = 0; i < pthreadcount; i++){
    pthread_join(tid[i], NULL);
}
```

### 3d ZIP file from shift3/hartakarun

to zip file we use `system` from shift3/hartakarun to server

```c
system("zip hartakarun.zip -r /home/zufarrifqi/shift3/hartakarun");
```

### 3e sent file from client to server using "send hartakarun.zip"

We use `argv[]` for command and then we check if the command is correct.

```c

//if number of argument is not correct
if(argc != 3){
    perror("Invalid number of arguments\n");
    return -1;
}
//if command is not "send"
if(strcmp(argv[1],"send") != 0){
    perror("Invalid command\n");
    return -1;
}
//if file doesn't exist
if(access(argv[2], F_OK) == -1){
    perror("File not found\n");
    return -1;
}
```

```
./client send hartakarun.zip
```

Client Socket :

```c
if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("\n Socket creation error \n");
    return -1;
}
printf("socket created\n");
```

Client Address :

```c
memset(&serv_addr, '0', sizeof(serv_addr));
serv_addr.sin_family = AF_INET;
serv_addr.sin_port = htons(PORT);
if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
    perror("\nInvalid address/ Address not supported \n");
    return -1;
}
printf("address created\n");

```

Client Connect :

```c
int con = connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
if(con < 0){
    perror("Connection failed\n");
    return -1;
}
printf("Connected to the server\n");
```

Create socket server :

```c
if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    perror("socket failed");
    exit(EXIT_FAILURE);
    }
printf("Socket successfully created\n");
```

set socket server :

```c
if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
    perror("setsockopt");
    exit(EXIT_FAILURE);
}
```

Create adress and bind server :

```c
address.sin_family = AF_INET;
address.sin_addr.s_addr = INADDR_ANY;
address.sin_port = htons( PORT );
if(bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0){
    perror("bind failed");
    exit(EXIT_FAILURE);
}
printf("Socket successfully binded\n");
```

Server Listening :

```c
if (listen(server_fd, 3) < 0) {
    perror("listen");
    exit(EXIT_FAILURE);
}
```

Server Accepting

```c
if ((new_socket = accept(server_fd, (struct sockaddr *)NULL, NULL))<0) {
    perror("accept");
    exit(EXIT_FAILURE);
}
printf("Socket successfully accepted\n");
```

to send file to server we need to read each data from the file and assign it buffer. Then we use `send` to send buffer to the socket.

```c
FILE *fp = fopen(filename, "r");
if(fp == NULL){
    perror("Error in reading file\n");
    return -1;
}

send(sock, filename, strlen(filename), 0);
while((valread = fread(buffer, 1, sizeof(buffer), fp))> 0){
    if(send(sock, buffer, valread, 0) < 0){
        perror("Error in sending file\n");
        return -1;
    }
    bzero(buffer, sizeof(buffer));
}
```

In server because we dont have hartakarun.zip file we need to write new file where we got the filename from client. then we use `recv` to receive the data from client and write it to the file

```c
FILE *fp = fopen(buffer, "wb");
while((b = recv(new_socket, buffer, 1024, 0)) > 0){
    if(fwrite(buffer, 1, b, fp) < 0){
        printf("Error writing to file\n");
        return -1;
        }
    bzero(buffer, 1024);
}
```

result of folder server :
![server](https://cdn.discordapp.com/attachments/783723240098889732/964484760909021234/unknown.png)

## Revisi

- Add join_thread to soal3.c
- Add answer to soal 3d - 3e


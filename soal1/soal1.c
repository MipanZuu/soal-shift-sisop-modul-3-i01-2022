#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <syslog.h>
#include <pwd.h>
#include <grp.h>
#include <stdint.h>
#include <string.h>

pthread_t tid[6];
pid_t child;

#define WHITESPACE 64
#define EQUALS 65
#define INVALID 66


static const unsigned char d[] = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 66, 66, 66, 65, 66, 66, 66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66, 66, 66, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66
    };

int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen){
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end){
        unsigned char c = d[*in++];

        switch (c){
        case WHITESPACE:
            continue; /* skip whitespace */
        case INVALID:
            return 1; /* invalid input, return error */
        case EQUALS:  /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iteration
            /* If the buffer is full, split it into bytes */
            if (iter == 4){
                if ((len += 3) > *outLen)
                    return 1; /* buffer overflow */
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3){
        if ((len += 2) > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2){
        if (++len > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}



void norepeatfork (char execute[], char *variable_path[]) 
{ 
    int status; 
    pid_t child_id;
    child_id = fork();

    if(child_id == 0)
    {
        execv(execute, variable_path);
    }

    else
    {
        ((wait(&status))>0);
        return; 
    } 
}


void *unzipQuotes( void *ptr){
    char destination[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote";
    char quotefold[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/quote.zip";
    char quote[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote";

    pid_t child_id;
    char path[201];
    int status;
    child_id = fork();

    strcpy(path, destination);
    strcat(path, "/quote");



    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldquo[] = {"mkdir","-p", quote, NULL};
        norepeatfork("/bin/mkdir", createfoldquo);

        char *unzipTheQ[] = {"unzip", "-q", quotefold, "-d", destination, NULL};
        norepeatfork("/usr/bin/unzip", unzipTheQ);
    }
    else {
        while ((wait(&status))>0);
    }
    return NULL;
}

void *unzipMusic(void *ptr){
    char destination[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music";
    char musicfold[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/music.zip";
    char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music";
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldmus[] = {"mkdir","-p", music, NULL};
        norepeatfork("/bin/mkdir", createfoldmus);

        char *unzipTheM[] = {"unzip", "-q", musicfold, "-d", destination, NULL};
        norepeatfork("/usr/bin/unzip", unzipTheM);
    }
    else {
        while ((wait(&status))>0);
    }
}

// void *decodeMusic(void *ptr){
//     FILE* fp;
//     char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt";
//     char destinationm[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/";
//     char path[151];
//     strcpy(path, destinationm);
//     strcat(path, "music.txt");
//     fp = fopen(path, "a");
    
//     for(int i=1; i<10; i++){
//     char *decode[]={"base64 -d /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/m%d.txt >> /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt", i};
//     norepeatfork("/usr/bin/base64", decode);
//     }
//     // for(int i=1; i<10; i++){
//     //         char cmd[120];
//     //         sprintf(cmd, "base64 -d /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/q%d.txt >> /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/quote.txt", i);
//     //         base64(cmd);
//     //     }
// }

// void *decodeQuote(void *ptr){
//     FILE* fp;
//     char quote[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/quote.txt";
//     char destinationq[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/";
//     char path[151];
//     strcpy(path, destinationq);
//     strcat(path, "quote.txt");
//     fp = fopen(path, "a");
    
//     char *decode[]={"base64", "-d", destinationq, NULL};
//     norepeatfork("/usr/bin/base64", decode);

//     fprintf(fp, "%s\n", decode);
// }

// void base64(char* text){
//     int status;
//     pid_t child_id = fork();

//     if(child_id < 0){
//         exit(EXIT_FAILURE);
//     }
//     else if(child_id == 0){
//         execl("/bin/sh", "sh", "-c", text, (char *)0);
//     }
//     else{
//         while((wait(&status)) > 0);
//     }
// }

// void newLine(char* dir){
//     int status;
//     pid_t child_id = fork();

//     if(child_id < 0){
//         exit(EXIT_FAILURE);
//     }
//     else if(child_id == 0){
//         char text[100] = "echo "" >> ";
//         strcat(text, dir);
//         execl("/bin/sh", "sh", "-c", text, (char *)0);
//     } 
//     else{
//         while((wait(&status)) > 0);
//     }
// }

void decodeBase64(char *name){
    DIR *dp;
    if ((dp = opendir(name)) != NULL){
        struct dirent **namelist;
        int n;

        n = scandir(name, &namelist, NULL, alphasort);
        int i = 0;
        if (n == -1)
            perror("scandir");

        for (int i = 0; i < n; i++){
            if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
                char *decoded;
                char dirName[100];
                strcpy(dirName, name);
                strcat(dirName, "/");
                strcat(dirName, namelist[i]->d_name);
                free(namelist[i]);

                FILE *fp = fopen(dirName, "r");
                char line[100];
                while (fgets(line, 100, fp) != NULL){
                    char *in = line;
                    size_t inLen = strlen(in);
                    size_t outLen = inLen;
                    decoded = malloc(sizeof(char) * 32768);
                    base64decode(in, inLen, (unsigned char *)decoded, &outLen);
                }
                fclose(fp);

                char txtDir[100];
                strcpy(txtDir, name);
                strcat(txtDir, "/");
                strcat(txtDir, name);
                strcat(txtDir, ".txt");

                fp = fopen(txtDir, "a");
                fprintf(fp, "%s\n", decoded);
                fclose(fp);
            }
        }
        free(namelist);
        closedir(dp);
    }
}

void *decodeMainM(){
        decodeBase64("music");
    return NULL;
}

void *decodeMainQ(){
        decodeBase64("quote");
    return NULL;
}


// void* decode(){
//     pthread_t id = pthread_self();

//     if(pthread_equal(id, tid[2])){
//     FILE* fp;
//     char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt";
//     char destinationm[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/";
//     char path[151];
//     strcpy(path, destinationm);
//     strcat(path, "music.txt");
//     fp = fopen(path, "a");
//         for(int i=1; i<10; i++){
//             char cmd[120];
//             sprintf(cmd, "base64 -d /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/m%d.txt >> /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt", i);
//             base64(cmd);
//             // newLine("/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/music.txt");
//         }
//     }
//     else if(pthread_equal(id, tid[3])){
//         for(int i=1; i<10; i++){
//             char cmd[120];
//             sprintf(cmd, "base64 -d /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/q%d.txt >> /Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/quote.txt", i);
//             base64(cmd);
//             newLine("/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/quote.txt");
//         }
//     }
// }


void *Createhasil(){
    char hasil[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil";
    pid_t child_id;
    char path[201];
    int status;
    child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0)
    {
        char *createfoldmus[] = {"mkdir","-p", hasil, NULL};
        norepeatfork("/bin/mkdir", createfoldmus);

    }
    else {
        while ((wait(&status))>0);
    }
}

void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "mv", "{}", destination, ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void *next(){
    pid_t child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char hasil[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil";
        char quote[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/quote/";
        char music[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/music/";
        move(quote, hasil, "quote.txt");
        move(music, hasil, "music.txt");    
    }
    else{
        wait(NULL);
    }
}

void zip(char *name, char *password){
    char fileName[100];
    strcpy(fileName, name);
    strcat(fileName, ".zip");

    char src[100];
    strcpy(src, name);
    strcat(src, "/*");

    char *argv[] = {"zip", "-P", password, "-r", "-j", fileName, name, NULL};
    execv("/usr/bin/zip", argv);
}

void DeleteDirectory(char *name){
    char *argv[] = {"rm", "-rf", name, NULL};
    execv("/bin/rm", argv);
}

void addingNo(char *name){
    FILE *fp = fopen(name, "w");
    fprintf(fp, "No\n");
    fclose(fp);
}

void unzipWithPassword(char *fileName, char *password){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", "-P", password, fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}


void *zipMain(){
    char password[100] = "mihinomenest";
    char *user = getenv("USER");
    strcat(password, user);

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[6])){
        int status;
        pid_t child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                zip("hasil", password);

            else{
                while ((wait(&status)) > 0);
                pid_t child_id2 = fork();

                if (child_id2 == 0)
                    DeleteDirectory("hasil");

                else{
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 == 0)
                        unzipWithPassword("hasil.zip", password);

                    else{
                        while ((wait(&status)) > 0);
                        addingNo("hasil/no.txt");
                        zip("hasil", password);
                        DeleteDirectory("hasil");
                    }
                }
            }
        }
    }
    return NULL;
}





// void zipp(){
//     char hasil[101] = "hasil";
//     char hasildel[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil";
//     char password[101]="test123";
//     pid_t child_id = fork();
//     if (child_id < 0)
//     {
//         exit(EXIT_FAILURE);
//     }
//     if(child_id == 0){
//         char *zip[]={"zip", "--password", password, "-r", "hasil.zip", hasil, NULL};
//         norepeatfork("/usr/bin/zip", zip);

//         rmdir(hasil);

//         char *rem[]={"rm", "-rf", hasildel, NULL};
//         norepeatfork("/bin/rm", rem);
//     }
//     else{
//         wait(NULL);
//     }
// }

// void unzipadd(){
//     char destination[101] = "/Users/dentabramasta/Modul_3/soal-shift-sisop-modul-3-i01-2022/soal1/hasil.zip";
//     pid_t child_id;
//     int status;
//     child_id = fork();

//     if (child_id < 0)
//     {
//         exit(EXIT_FAILURE);
//     }
//     if (child_id == 0)
//     {
//         char *unzipTheM[] = {"unzip", destination, NULL};
//         norepeatfork("/usr/bin/unzip", unzipTheM);
//     }
//     else {
//         while ((wait(&status))>0);
//     }
// }


int main () {
    int thread;
    thread = pthread_create(&tid[0], NULL, unzipQuotes, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 0\n");
    
    thread = pthread_create(&tid[1], NULL, unzipMusic, NULL);
    if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    
    printf("pthread_create() success for thread 1\n");

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    thread = pthread_create(&tid[2], NULL, decodeMainM, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 2\n");


    thread = pthread_create(&tid[3], NULL, decodeMainQ, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 3 \n");
    pthread_join(tid[2], NULL);
    pthread_join(tid[3], NULL);

    thread = pthread_create(&tid[4], NULL, Createhasil, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 4 \n");

    thread = pthread_create(&tid[5], NULL, next, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 5 \n");
    pthread_join(tid[4], NULL);
    pthread_join(tid[5], NULL);

    thread = pthread_create(&tid[6], NULL, zipMain, NULL);
     if(thread) //jika eror
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",strerror(thread));
        exit(EXIT_FAILURE);
    }
    printf("pthread_create() success for thread 6 \n");
    pthread_join(tid[6], NULL);

    return 0;
}
